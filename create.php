<?php

/**
 * This file is part of PHP Generators.

    PHP Generators is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PHP Generators is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PHP Generators.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * The purpose of this program is to create generators for 4 things,
 * 1) WordPress Themes
 * 2) WordPress Plugins
 * 3) Drupal Themes
 * 4) Drupal Plugins
 * 
 * Hopefully, more will come later, but for now, these four are the goal.
 * 
 */

require_once "autoload.php";

$prgm = new kkeiper1103\Generators\Generator( $_SERVER['argc'], $_SERVER['argv'] );

$prgm->run();
