<?php

namespace kkeiper1103\Generators;

class ErrorCode
{
    
    public static $PlatformNotRecognized = 101;
    
    public static $CommandNotRecognized = 101;
    
    public static $ClassNotFound = 102;
    
    public static $FileNotFound = 103;
    
    public static $NotEnoughArguments = 104;
    
    public static $TemplatePathIsNull = 105;
    
}
