<?php

namespace kkeiper1103\Generators;

use \Exception;

class Generator{
    
    protected $argc;
    
    protected $_argv; // original argv value
    protected $argv; // parsed and friendly argv values
    
    public function __construct( $argc, $argv )
    {
        $this->argc = $argc;
        $this->_argv = $argv;
        
        //$this->_register_autoloaders();
    }
    
    public function run()
    {
        $this->_parse_args();
       
        
        try
        {
            
            $this->_enforce_args_given();
            
            
            $class = ucwords($this->argv['command']['class']);
            
            $full = "\\kkeiper1103\\Generators\\Platforms\\$class";
            $method = ucwords($this->argv['command']['method']);
            
            if( class_exists($full) )
            {
                
                
                $instance = new $full();
                
                if( method_exists($instance, $method) )
                {
                    return call_user_func_array(array($instance, $method), array(
                        'name' => $this->argv['name'],
                        'extras' => $this->argv['arbitrary_arguments']
                    ));
                }
                else {
                    throw new Exception("Command {$method} for {$full} Not Recognized!", ErrorCode::$CommandNotRecognized);
                }
            }
            else
            {
                throw new Exception("Platform {$full} Not Recognized!", ErrorCode::$PlatformNotRecognized );
            }
            
            
        }
        catch(Exception $e)
        {
            echo <<<EOE
\033[1;37m\033[41m# Exception Encountered! ############
Exception Code: {$e->getCode()}
Exception Message: {$e->getMessage()}
# End of Exception ##################\n\033[0m
EOE;
        }
        
    }
    
    /***** Private Methods *****/
    
    protected function _parse_args()
    {
        /**
         * Parse given command
         */
         
        // start from offset 1 because the 0th arg is the script name
        
        $args = explode(":", $this->_argv[1]);
        
        $args['class'] = $args[0];
        $args['method'] = $args[1];
        
        unset( $args[0], $args[1] );
        
        $this->argv['command'] = $args;
            
        /**
         * Get name of Created Module/Theme
         */
        
        $this->argv['name'] = $this->_argv[2];
        
        /**
         * Get any arbitrary arguments now
         */
         
        $arb_args = [];
        for( $i = 3; $i < sizeof($this->_argv); $i++)
        {
            $arb_args[] = $this->_argv[$i];
        }
        
        $this->argv['arbitrary_arguments'] = $arb_args;
    }
    
    protected function _enforce_args_given()
    {
        /*for( $i = 0; $i < 3; $i++ )
        {
            print_r( $this->args );
            
            if( ! isset($this->args[$i]) )
            {
                throw new Exception("Not Enough Arguments Given", ErrorCode::$NotEnoughArguments);
            }
        }*/
    }
    
    /*
    protected function _register_autoloaders()
    {
        spl_autoload_register(function($classname){
            
            $directories = array(
                'src',
                'src/exceptions',
                'src/interfaces',
                'src/platforms',
                'src/traits'
            );
            
            $found = false;
            foreach($directories as $dir)
            {
                $file = $dir . DIRECTORY_SEPARATOR . $classname . ".php";   
                if( file_exists($file) )
                {
                    $found = true;
                    require_once $file;
                }
            }
            
            if(!$found)
                throw new Exception("Class/Interface {$classname} Could Not Be Loaded!", ErrorCode::$ClassNotFound);
            
        });
    }
    */
}
