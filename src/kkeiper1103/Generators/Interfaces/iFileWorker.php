<?php

namespace kkeiper1103\Generators\Interfaces;

interface iFileWorker
{
    public function ReadFile( $filename );
    public function WriteFile( $filename, $content );
    
    public function CopyFile( $filesrc, $filedest );
    public function MoveFile( $filesrc, $filedest );
    
    public function CreateFile( $filename );
    public function DeleteFile( $filename );
    
    public function MakeDirectory( $directory );
}
