<?php

namespace kkeiper1103\Generators\Platforms;

class Drupal extends Platform
{
    
    public function Theme( $name )
    {
        $this->PrintString("Making Drupal Theme '$name'...", "white", "blue");
        
        
        $this->MakeDirectory( $this->out . DIRECTORY_SEPARATOR . "themes" . DIRECTORY_SEPARATOR . $name );
         
        
    }
    
    public function Module( $name )
    {
        $this->PrintString("Making Drupal Module '$name'...", "white", "blue");
       
       
        $this->MakeDirectory( $this->out . DIRECTORY_SEPARATOR . "modules" . DIRECTORY_SEPARATOR . $name );
         
        
    }
    
}
