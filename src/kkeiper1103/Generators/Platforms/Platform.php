<?php

namespace kkeiper1103\Generators\Platforms;

use \Exception,
    \kkeiper1103\Generators\Interfaces\iFileWorker,
    \kkeiper1103\Generators\Traits\ColorPrinter;


class Platform implements iFileWorker
{
    use ColorPrinter;
    
    protected $out = null;
    
    public function __construct()
    {
        $name = end(explode( "\\", get_class($this) ));
        
        $this->out = "OUTPUT" . DIRECTORY_SEPARATOR . $name;
    }
    
    
    
    /**
     * Implements iFileWorker Methods
     */
    
    public function ReadFile( $filename ){
        if( file_exists($filename) )
        {
            return file_get_contents($filename);
        }
        else
            throw new Exception("File {$filename} Was Not Found.", ErrorCode::$FileNotFound);
    }
    
    public function WriteFile( $filename, $content, $append = false ){
        if( is_array($filename) )
        {
            foreach( $filename as $f )
            {
                $this->WriteFile( $f, $content );
            }
        }
        else
        {
            if( file_exists($filename) && $append)
            {
                return file_put_contents($filename, $content, FILE_APPEND|LOCK_EX);
            }
            else
                return file_put_contents($filename, $content, LOCK_EX);
        }
    }
    
    public function CopyFile( $filesrc, $filedest ){
        if( !file_exists($filesrc) )
            throw new Exception( "File {$filesrc} Was Not Found.", ErrorCode::$FileNotFound );
        
        return file_put_contents( $filedest, file_get_contents($filesrc) );
        
    }
    public function MoveFile( $filesrc, $filedest ){
        if( !file_exists($filesrc) )
            throw new Exception( "File {$filesrc} Was Not Found.", ErrorCode::$FileNotFound );
        
        $contents = file_get_contents($filesrc);
        $result = file_put_contents( $filedest, $contents );
        
        // iff successfully moved, then delete src file
        if($result)
            unlink($filesrc);
        
        return $result;
    }
    
    public function CreateFile( $filename ){
        return $this->WriteFile($filename, "");
    }
    public function DeleteFile( $filename ){
        if( !file_exists($filename) )
            throw new Exception( "File {$filesrc} Was Not Found.", ErrorCode::$FileNotFound );
        
        return unlink($filename);
    }
    
    public function MakeDirectory( $directory )
    {
        if( is_dir($directory) )
        {
            if( ! $this->_prompt("$directory Already Exists! Overwrite? [y/N]"))
            {
                $this->PrintString("Aborting..", "blue", "white");
                return false;
            }
            else {
                
                $this->PrintString( "Overwriting Directory...", "blue", "white" );
                
                foreach( glob($directory . DIRECTORY_SEPARATOR . "*") as $file )
                {
                    unlink($file);
                }
                
                rmdir($directory);
                
                $this->PrintString( "Done Overwriting Directory.", "blue", "white" );
            }
        }
        
        return mkdir( $directory, 0755, true );
    }
    
    
    protected function GetFriendlyName( $name )
    {
        return ucwords(
            // insert space between camelCase, ie camelCase => camel Case
            preg_replace( '/([a-z])([A-Z])/', '$1 $2', str_replace( array("-", "_", "+") , " ", $name))
        );
    }
    
    protected function GetCamelCaseName( $name )
    {
        // deal with array input
        if( is_array($name) )
        {
            $final = array();
            
            foreach($name as $part)
                $final[] = ucwords($part);
            
            return implode("", $final);
        }
        
        $name = str_replace( array("-", "_", "+"), " ", $name );
        
        $name = ucwords($name);
        
        return str_replace( " ", "", $name);
    }
    
}
