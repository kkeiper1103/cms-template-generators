<?php

namespace kkeiper1103\Generators\Platforms;

use \Exception,
	\kkeiper1103\Generators\ErrorCode;

class Wordpress extends Platform
{
    
    public function Theme( $name, $extra_files = array() )
    {
        $this->PrintString("Making WordPress Theme '$name'...", "white", "blue");
        
        /**
         * Let's Create the WordPress Theme with all the necessary files.
         */
         $path = $this->out . DIRECTORY_SEPARATOR . "themes" . DIRECTORY_SEPARATOR . $name;
         $this->MakeDirectory( $path );
         
         // make styles and scripts dir
         $this->MakeDirectory( $path . DIRECTORY_SEPARATOR . "scripts" );
         $this->MakeDirectory( $path . DIRECTORY_SEPARATOR . "styles" );
         
         $files = ['404.php', 'category.php', 'scripts' . DIRECTORY_SEPARATOR . "main.js"];
         
         foreach($files as $file)
         {
             $this->WriteFile( $path . DIRECTORY_SEPARATOR . $file, <<<EOF
            <?php get_header(); ?>
            <?php while( have_posts() ): the_post(); ?>
            <div>
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </div>
            <?php endwhile; ?>
            <?php get_footer(); ?>
EOF
);
         }
         
         /**
          * Create Stylesheet
          */
         
         $friendly_name = $this->GetFriendlyName($name);
         
         $this->WriteFile( $path . DIRECTORY_SEPARATOR . "style.css", <<<EOL
/*
Theme Name: $friendly_name
Theme URI:
Description: 
Author:
Author URI:
Version:
Tags:

License: GPLv3
License URI: <http://www.gnu.org/licenses/gpl-3.0.txt>

Comments Here (optional).
*/
EOL
);
          
          /**
           * Write functions file
           */
         
         $this->WriteFile( $path . DIRECTORY_SEPARATOR . "functions.php", <<<EOL
<?php

/**
 * Functions File for $friendly_name
 * TODO: Write Functions For Theme
 */

class {$name}
{
    public function init()
    {
        \$this->stylesheets();
        \$this->javascripts();
        
        return $this;
    }

    public function stylesheets()
    {
        wp_enqueue_stylesheet( '$name-css', '<?php echo get_stylesheet_directory_uri(); ?>/styles/style.css' );
        return $this;
    }
     
    public function javascripts()
    {
        wp_enqueue_script( '$name-js', '<?php echo get_stylesheet_directory_uri(); ?>/scripts/main.js' );
        return $this;
    }
}

add_action('init', array( new $name(), 'init' ));

EOL
);
         
         /**
          * Write Header File
          */
          
         $this->WriteFile( $path . DIRECTORY_SEPARATOR . "header.php", <<<EOH
<!DOCTYPE html>
<html lang='en' <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title(); ?></title>
        <meta name='description' content='<?php bloginfo( 'description' ); ?>' />
        
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        
        <?php wp_head(); ?>
    </head>
    <body>
        <header id='header'>
            <nav id='navigation'>
                <?php
                    \$defaults = array(
                        'container' => 'div',
                        'menu_class' => 'menu',
                        'echo' => true,
                        'fallback_cb' => 'wp_page_menu',
                        'items_wrap' => '<ul id="%1\$s" class="%2\$s">%3\$s</ul>'
                    );
                    
                    wp_nav_menu( \$defaults );
                ?>
            </nav>
        </header>
        <div id='main-content'>
EOH
);
         
         /**
          * Write Index File
          */
          
         $template_files = array(
            $path . DIRECTORY_SEPARATOR . "index.php",
            $path . DIRECTORY_SEPARATOR . "page.php"
         );
          
         $this->WriteFile( $template_files, <<<EOI
        <?php get_header(); ?>
        <?php if( have_posts() ): ?>
            <?php while( have_posts() ): the_post(); ?>
                <div class='content entry'>
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                </div>
            <?php endwhile; ?>
        <?php else: ?>
        <div>
            <p>There Were No Posts Found</p>
        </div>
        <?php endif; ?>
        <?php get_footer(); ?>
EOI
);
         
         /**
          * Write Single Post File
          */
          
         $this->WriteFile( $path . DIRECTORY_SEPARATOR . "single.php", <<<EOF
        <?php get_header(); ?>
        <?php while( have_posts() ): the_post(); ?>
        <div class='entry' id='post-<?php the_ID(); ?>'>
            <h2><?php the_title(); ?></h2>
            <?php the_content(); ?>
            <div class='meta'>
                <p><?php the_category(', '); ?></p>
                <p>By: <?php the_author(); ?> <?php edit_post_link( __("Edit"), "<strong>&lt;|</strong>", "<strong>|&gt;</strong>" ); ?></p>
            </div>
        </div>
        <?php endwhile; ?>
        <?php get_footer(); ?>
EOF
);
         
         /**
          * Write Footer file
          */
         
         $this->WriteFile( $path . DIRECTORY_SEPARATOR . "footer.php", <<<EOF
        </div>
        <footer id='footer'>
            <!-- Your Copy(right/left) Here -->
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>
EOF
);
         
         /**
          * Process extra files
          */
         
         foreach($extra_files as $file)
         {
             $this->CreateFile( $path . DIRECTORY_SEPARATOR . $file );
         }
         
    } /** End Theme Method **/
    
    /**
     * @method Plugin
     */
    
    public function Plugin( $name, $extra_files = array() )
    {
        $this->PrintString("Making WordPress Plugin '$name'...", "white", "blue");
        
        /**
         * Let's Create the WordPress Plugin with all the necessary files.
         */
         
         $path = $this->out . DIRECTORY_SEPARATOR . "plugins" . DIRECTORY_SEPARATOR . $name;
         
         $this->MakeDirectory( $path );
         
         // move into the new plugin directory
         $orig_path = getcwd();
         chdir($path);
         
         // make the necessary auxiliary folders
         $other = array(
            "screens",
            "images"
         );
         
         foreach($other as $folder)
            $this->MakeDirectory( $folder );
         
         $friendly_name = $this->GetFriendlyName( $name );
         
         // write main plugin file.
         $cName = $this->GetCamelCaseName($name);
         $this->WriteFile( $name . ".php", <<<EOL
<?php
/*
Plugin Name: $friendly_name
Plugin URI: {URI}
Description: {DESC}
Version: 1.0 
Author: {AUTHOR}
Author URI: {Author URI}
*/

// To-Do: Write Plugin

class {$cName} {
            
    public function __construct()
    {}
    
    public function __destruct()
    {}
    
    /**
     * Admin Initialization
     */
    public function admin()
    {
        
    }
    
    /**
     * Menu Declaration
     */
    public function menu()
    {
        // add MainLevel option page
        add_menu_page(
            "{$friendly_name} - Admin", "{$friendly_name}", 
            "manage_options", 
            "{$name}/screens/admin.php", '', 
            "http://placehold.it/16x16/000000/ffffff&text=K", //plugins_url("{$name}/images/icon.png"), 
            100
        );
        
    }
    
    /**
     * Initialize Plugin
     */
     
    public function init()
    {
        
    }
    
}

\$instance = new $cName();

add_action( "admin_init", array( \$instance, "admin" ) );
add_action( "admin_menu", array( \$instance, "menu" ) );

// non-authorized actions
add_action( "init", array( \$instance, "init" ) );

EOL
);

        // write all auxiliary files needed, like admin.php, menu.php, etc
        $files = array(
            "screens/admin.php",
        );
        
        $this->LoadTemplate( __DIR__ . "/Templates/WordPressAdmin.php" );
        
        foreach($files as $file)
            $this->WriteFile( $file, "" );


         // Create extra files provided by generator
         foreach($extra_files as $file)
         {
             $filename = strtolower($file);
             $class = ucfirst($file);
             
             $this->WriteFile( $filename . ".php", <<<EOL
<?php

class {$class} {
            
    public function __construct()
    {}
    
    public function __destruct()
    {}
    
    public function init()
    {
        // start logic
    }
    
}

// \$obj = new {$class}();
// \$obj->init();

EOL
);
         }
         
         // restore original working dir
         chdir($orig_path);
         
    }
    
    /**
     * Loads Template File and Uses It to write Final File
     * 
     * @return string
     */
    public function LoadTemplate( $path )
    {
    	// some error prevention checking
    	
    	if( is_null($path) )
    	{
    		throw new Exception("Need Template File Path!", ErrorCode::$TemplatePathIsNull);
    	}
    	if( !is_readable( $path ) )
    	{
    		throw new Exception("File \"{$path}\" Could Not Be Found!", ErrorCode::$FileNotFound);
    	}
    	
    	// since we have a valid path now
    	// let's load that template and return the rendered contents
    	
    	$contents = $this->ReadFile( $path );
    	
    	$pattern = "@\[\s*([^\s]*)\s*\]@";
    	$replacement = "\\1";
    	
    	$contents = preg_replace(
        	$pattern, $replacement,
    		$contents
    	);
    	
    	echo $contents;
    	
    }
    
}
