<?php

namespace kkeiper1103\Generators\Traits;

trait ColorPrinter
{
    public $fg_colors = array(
        "black" => "0;30",
        "dark_gray" => "1;30",
        "blue" => "0;34",
        "light_blue" => "1;34",
        "green" => "0;32",
        "light_green" => "1;32",
        "cyan" => "0;36",
        "light_cyan" => "1;36",
        "red" => "0;31",
        "light_red" => "1;31",
        "purple" => "0;35",
        "light_purple" => "1;35",
        "brown" => "0;33",
        "yellow" => "1;33",
        "light_gray" => "0;37",
        "white" => "1;37"
    );
    public $bg_colors = array(
        "white" => "40",
        "red" => "41",
        "green" => "42",
        "yellow" => "43",
        "blue" => "44",
        "magenta" => "45",
        "cyan" => "46",
        "light_grey" => "47"
    );
    
    public function PrintString($text, $fg = null, $bg = null){
        $final_text = "";
        
        if( ! is_null($fg) )
        {
            $final_text .= "\033[{$this->fg_colors[$fg]}m";
        }
        
        if( ! is_null($bg) )
        {
            $final_text .= "\033[{$this->bg_colors[$bg]}m";
        }
        
        echo $final_text .= "$text\033[0m\n";
        
    }
    
    public function _prompt( $text )
    {
        $this->PrintString( $text, "blue", "white");
        
        $confirmation = ["y", "yes"];
        $user_value = trim(fgets(STDIN));
        $res = in_array(strtolower($user_value), $confirmation);
        
        return $res;
    }
}
